def fac_recursive(n, P):
    if n < P:
        return []
    if n % P == 0:
        return [P] + factorization(n/P, 2)
    return factorization(n, P + 1)


import math

def fac(x):
    a = x
    res = []

    while a > 1:
        until = int(math.sqrt(a)) + 1
        for i in range(2, until):
            if a % i == 0:
                res.append(i)
                a /= i
                break
        else:
            res.append(int(a))
            return res
    return res
