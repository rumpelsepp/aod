import math

lim = 1000
lim_sqrt = int(math.sqrt(lim)) + 1
primes = [2]
is_prime = [True for i in range(0, lim)]

for i in range(3, lim, 2):
    if is_prime[i]:
        if i <= lim_sqrt:
            for j in range(i**2, lim, i):
                is_prime[j] = False
            primes.append(i)
        else:
            primes.append(i)

print(primes)
