data = [9, 5, 3, 1, 4, 10, 20, 21]

for i in range(0, len(data)):
    for j in range(0, len(data)-i-1):
        if data[j+1] < data[j]:
            k = data[j+1]
            data[j+1] = data[j]
            data[j] = k

print(data)
