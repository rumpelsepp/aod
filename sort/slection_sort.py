data = [9, 5, 3, 3, 78, 214, 123543652, 234, 45, 1, 4, 10, 20, 21, 3, 4]

for i in range(0, len(data)):
    k = i
    for j in range(i+1, len(data)):
        if data[j] < data[k]:
            k = j
    data[k], data[i] = data[i], data[k]

print(data)
