data = [9, 5, 3, 1, 4, 10, 20, 21]

for i in range(0, len(data)):
    j = i
    while j > 0 and data[j-1] > data[j]:
        k = data[j-1]
        data[j-1] = data[j]
        data[j] = k
        j -= 1

print(data)
