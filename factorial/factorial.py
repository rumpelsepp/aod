def fak(n):
    res = 1
    for i in range(1, n+1):
        res = res * i
    return res


def fak_rec(n):
    if n == 0 or n == 1:
        return 1
    if n > 1:
        return n * fak_rec(n-1)


def fak_tail(n, a):
    if n == 0 or n == 1 :
        return a
    if n > 1:
        return fak_tail(n-1, n*a)

