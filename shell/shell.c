#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <signal.h>
#include <stdbool.h>
#include "builtin.h"

#define BUFSIZE 1024

struct shell_state {
	int last_return;
	char *cwd;
};

struct shell_state * init_state() {
	struct shell_state *state = malloc(sizeof(struct shell_state));
	state->cwd = get_current_dir_name();
	state->last_return = 0;

	return state;
}

void update_state(struct shell_state *state, int ret) {
	free(state->cwd);
	state->cwd = get_current_dir_name();
	state->last_return = ret;
}

void free_state(struct shell_state *state) {
	free(state->cwd);
	free(state);
}

void print_prompt(struct shell_state *state) {
	char *user_indicator = malloc(sizeof(char));

	if (geteuid() == 0) {
		strcpy(user_indicator, "#");
	} else {
		strcpy(user_indicator, "$");
	}

	if (state->last_return == 0) {
		printf("%s %s ", state->cwd, user_indicator);
	} else {
		printf("%s [%d] %s ", state->cwd, state->last_return, user_indicator);
	}

	free(user_indicator);
}

bool strempty(const char *s) {
	// http://stackoverflow.com/a/7970669
	if ((s != NULL) && (s[0] == '\0')) {
		return true;
	}
	return false;
}

char **split_args(char **s) {
	int i = 0;
	char *word;
	char **argv = (char **) malloc(sizeof(char *));

	while ((word = strsep(s, " ")) != NULL) {
		if (strempty(word)) {
			continue;
		}

		argv = (char **) realloc(argv, sizeof(char *) * (i + 1));
		argv[i] = strdup(word);
		i++;
	}

	argv = (char **) realloc(argv, sizeof(char *) * (i + 1));
	argv[i] = NULL;

	return argv;
}

void freeargs(char **argv) {
	int i = 0;

	while (argv[i] != NULL) {
		free(argv[i]);
		i++;
	}

	free(argv);
}

int main(int argc, const char *argv[]) {
	struct shell_state *state = init_state();
	char *cmd;
	char **cmd_argv;
	int last_return = 0;

	while (1) {
		pid_t child_pid;
		sigset_t mask, omask;
		char buffer[BUFSIZE];
		char *strptr;
		int ret;

		update_state(state, last_return);
		print_prompt(state);

		fgets(buffer, sizeof(buffer), stdin);
		// Strip newline char
		buffer[strlen(buffer)-1] = '\0';

		strptr = buffer;

		cmd_argv = split_args(&strptr);
		cmd = cmd_argv[0];

		if (strempty(cmd) || cmd == NULL) {
			freeargs(cmd_argv);
			continue;
		}

		if (strcmp(cmd, "exit") == 0) {
			goto exit;
		}

		ret = builtin_handle(cmd, cmd_argv);
		if (ret == 0){
			freeargs(cmd_argv);
			continue;
		}

		ret = blocksignal(SIGINT, &omask);
		if (ret <0) {
			perror("blocksignal");
			exit(EXIT_FAILURE);
		}

		child_pid = spawnproc(cmd_argv);
		if (child_pid < 0) {
			perror("spawnproc");
			exit(EXIT_FAILURE);
		}

		ret = setpgid(child_pid, child_pid);
		if (ret < 0) {
			perror("setpgid(3)");
			exit(EXIT_FAILURE);
		}

		ret = sigprocmask(SIG_SETMASK, &omask, NULL);
		if (ret < 0) {
			perror("sigprocmask(3)");
			exit(EXIT_FAILURE);
		}

		int status;
		waitpid(child_pid, &status, WUNTRACED);

		if (WIFEXITED(status)) {
			last_return = WEXITSTATUS(status);
		} else if (WIFSIGNALED(status)) {
			last_return = 128 + WTERMSIG(status);
		} else if (WIFSTOPPED(status)) {
			last_return = -1;
		} else {
			last_return = -2;
		}

		freeargs(cmd_argv);
	}

exit:
	freeargs(cmd_argv);
	free_state(state);

	return EXIT_SUCCESS;
}
