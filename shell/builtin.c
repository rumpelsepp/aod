#include <unistd.h>
#include <string.h>
#include <stdio.h>


void builtin_cd(char *p) {
	int ret = chdir(p);
	if (ret < 0) {
		perror("buildin cd");
	}
}

int builtin_handle(char *p, char **argv) {
	if (strcmp(p, "cd") == 0) {
		builtin_cd(argv[1]);
		return 0;
	}

	return -1;
}
