#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>


int blocksignal(int signum, sigset_t *omask) {
	int ret;
	sigset_t mask;

	ret = sigemptyset(&mask);
	if (ret < 0) {
		return -1;
	}

	ret = sigaddset(&mask, signum);
	if (ret < 0) {
		return -1;
	}

	ret = sigprocmask(SIG_BLOCK, &mask, omask);
	if (ret < 0) {
		return -1;
	}

	return 0;
}

size_t arglen(char **argv) {
	size_t i = 0;

	while (argv[i] != NULL) {
		i++;
	}

	return i;
}

pid_t spawnproc(char **argv) {
	int ret;
	size_t argc = arglen(argv);
	pid_t pid = fork();

	if (pid == 0) {
		int ret = setpgid(0, 0);
		if (ret < 0) {
			perror("setpgid(3)");
			_exit(EXIT_FAILURE);
		}

		if (argc == 1) {
			ret = execlp(argv[0], "", NULL);
		} else if (argc > 1) {
			ret = execvp(argv[0], argv);
		} else {
			return -1;
		}

		if (ret < 0) {
			perror("execve(2)");
			_exit(EXIT_FAILURE);
		}
	}

	return pid;
}
