use std::process;
use std::sync::{Arc, Mutex};
use clap::ArgMatches;
use walkdir::{DirEntry};
use chunker::buzhash;
use scanner;
use scanner::FSItem;
use stats::Statistics;
use utils::{number_of_cpus, human_readable_byte_count};


pub fn run<'a>(matches: &ArgMatches<'a>) {
    let dirs: Vec<_> = matches.values_of("SRC").unwrap().collect();
    let nthreads = match matches.value_of("THREADS") {
        Some(x) => x.trim().parse::<u32>().unwrap_or(1),
        None => number_of_cpus(),
    };

    // let mut rng = rand::thread_rng();
    // let seed = rng.gen::<u32>();
    let chunker_params = buzhash::ChunkerParams::new(buzhash::HASH_MASK_BITS,
                                                     buzhash::HASH_WINDOW_SIZE,
                                                     buzhash::CHUNK_MIN_EXP,
                                                     buzhash::CHUNK_MAX_EXP,
                                                     0x00); // TODO: Reenable seed

    let mut threads = vec![];
    let buffer: Vec<DirEntry> = Vec::new();
    let mut pathqueue = Arc::new(Mutex::new(buffer));

    info!("Spawning {} threads for creating filelist.", nthreads);
    for dir in dirs {
        let walker = try_spawn!(scanner::Walker::new(dir.to_string(), pathqueue.clone()));
        threads.push(walker);

        // Make sure that there are not more walkers than nthreads.
        while threads.len() > nthreads as usize {
            if let Some(thread) = threads.pop() {
                let _ = thread.join();
            }
        }
    }

    // Wait until all walkers have finished their work.
    while let Some(thread) = threads.pop() {
        let _ = thread.join();
    }

    {
        // This should panic in case of error. Something weird happened
        // when an error actually occurs.
        let mut pathqueue = Arc::get_mut(&mut pathqueue)
                                .expect("Could not unwrap pathqueue reference counter")
                                .get_mut()
                                .expect("Could not unwrap pathqueue mutex");

        if pathqueue.is_empty() {
            error!("No paths in queue. Did you try reading a non existing path?");
            process::exit(1);
        }

        info!("Finished directory listing.");
        info!("Sorting dir list by inode numbers.");
        pathqueue.sort_by_key(|k| k.ino());
        info!("Deduplicating dir list.");
        pathqueue.dedup_by_key(|k| k.path().to_owned());
    }

    let resbuffer: Vec<FSItem> = Vec::new();
    let items = Arc::new(Mutex::new(resbuffer));
    let mut stats = Arc::new(Mutex::new(Statistics::new()));

    info!("Spawning {} threads for processing files.", nthreads);
    for _ in 0..nthreads {
        let pathqueue = pathqueue.clone();
        let items = items.clone();
        let params = chunker_params.clone();
        let stats = stats.clone();
        let file_proc = try_spawn!(scanner::FileProcessor::new(pathqueue, items, params, stats));
        threads.push(file_proc);
    }

    // Wait until all threads have finished their work.
    while let Some(thread) = threads.pop() {
        let _ = thread.join();
    }

    let stats = Arc::get_mut(&mut stats)
                    .expect("Bug, there are references left!")
                    .get_mut()
                    .expect("Bug, could not unwrap mutex");

    println!("Stats summary");
    println!("-------------");
    println!("Files read: {}", stats.files_read);
    println!("Bytes read: {}", human_readable_byte_count(stats.bytes_read));
}
