use std::io;
use std::process;
use std::str::FromStr;
use clap::{ArgMatches, Shell};
use cli;

pub fn run<'a>(matches: &ArgMatches<'a>) {
    // .unwrap() is safe here, since clap varifies the existance of SHELL.
    let shell = match Shell::from_str(matches.value_of("SHELL").unwrap()) {
        Ok(x) => x,
        Err(e) => {
            println!("Invalid shell specified!");
            println!("{}", e);
            process::exit(1);
        }
    };

    cli::build().gen_completions_to(crate_name!(), shell, &mut io::stdout());
}
