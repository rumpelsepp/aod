#[macro_export]
macro_rules! try_spawn {
    ($e:expr) => (
        match $e.spawn() {
            Ok(x) => x,
            Err(e) => {
                error!("{}", e);
                continue;
            },
        }
    );
}

#[macro_export]
macro_rules! try_or_skip {
    ($e:expr) => (
        match $e {
            Ok(x) => x,
            Err(e) => {
                warn!("{}", e);
                continue;
            },
        }
    );
}
