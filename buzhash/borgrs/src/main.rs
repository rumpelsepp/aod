// extern crate rand;
extern crate memmap;
extern crate sha2;
#[macro_use]
extern crate clap;
extern crate walkdir;
#[macro_use]
extern crate log;
extern crate env_logger;

use std::process;
use std::env;

#[macro_use]
mod macros;
mod chunker;
mod scanner;
mod utils;
mod types;
mod cli;
mod stats;
// mod hasher;

fn main() {
    let app = cli::build();
    let matches = app.clone().get_matches();

    // -v, -vv, -vvv are convenient shortcuts for the logging setup.
    // Btw. the RUST_LOG stuff is awesome and does not suck like Go's
    // logging package... :)
    match matches.occurrences_of("v") {
        1 => env::set_var("RUST_LOG", format!("{}={}", crate_name!(), "warn")),
        2 => env::set_var("RUST_LOG", format!("{}={}", crate_name!(), "info")),
        3 => env::set_var("RUST_LOG", format!("{}={}", crate_name!(), "debug")),
        _ => {},
    }

    if env_logger::init().is_err() {
        println!("Error initializing logging system.");
        process::exit(1);
    }

    match matches.subcommand() {
        ("backup", Some(matches)) => {
            cli::backup::run(matches);
        },
        ("completions", Some(matches)) => {
            cli::completions::run(matches);
        },
        _ => {},
    }
}
