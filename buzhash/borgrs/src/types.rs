use std::sync::{Arc, Mutex};
use walkdir::DirEntry;
use scanner::FSItem;
use stats::Statistics;

pub type DirEntryVec = Arc<Mutex<Vec<DirEntry>>>;
pub type FSItemsVec = Arc<Mutex<Vec<FSItem>>>;
pub type StatsCounter = Arc<Mutex<Statistics>>;
