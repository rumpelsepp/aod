use std::io;
use std::io::{Error, ErrorKind};
use std::io::prelude::*;
use std::fs::{File, Metadata};
use std::thread;
use std::thread::JoinHandle as ThreadHandle;

use memmap::{Mmap, Protection};
use sha2::{Sha256, Digest};
use walkdir::{DirEntry, WalkDir};

use chunker::buzhash::{Chunker, ChunkerParams};
use types::*;


#[derive(Debug)]
pub struct Walker {
    start: String,
    // ignores: Foo
    dir_entries: DirEntryVec,
}

impl Walker {
   pub fn new(start: String, dir_entries: DirEntryVec) -> Self {
        Self {
            start: start,
            dir_entries: dir_entries,
        }
    }

    pub fn spawn(self) -> io::Result<ThreadHandle<()>> {
        thread::Builder::new().name("walker".to_string()).spawn(move || {
            for entry in WalkDir::new(self.start) {
                let entry = try_or_skip!(entry);
                let mut dir_entries = self.dir_entries.lock().unwrap();
                dir_entries.push(entry);
            }
        })
    }
}

#[derive(Debug)]
pub struct FSItem {
    path: DirEntry,
    metadata: Metadata,
    c_offsets: Option<Vec<usize>>,
    c_hashes: Option<Vec<Vec<u8>>>,
}

impl FSItem {
    pub fn new(path: DirEntry, metadata: Metadata) -> Self {
        Self {
            path: path,
            metadata: metadata,
            c_offsets: None,
            c_hashes: None,
        }
    }

    pub fn new_with_chunks(path: DirEntry, metadata: Metadata, c_offsets: Vec<usize>, c_hashes: Vec<Vec<u8>>) -> Self {
        Self {
            path: path,
            metadata: metadata,
            c_offsets: Some(c_offsets),
            c_hashes: Some(c_hashes),
        }
    }
}

pub struct FileProcessor {
    chunker_params: ChunkerParams,
    // hasher_params: &HasherParams,
    dir_entries: DirEntryVec,
    items: FSItemsVec,
    stats_counter: StatsCounter,
}

impl FileProcessor {
    pub fn new(dir_entries: DirEntryVec, items: FSItemsVec, chunker_params: ChunkerParams, stats_counter: StatsCounter) -> Self {
        Self {
            dir_entries: dir_entries,
            items: items,
            chunker_params: chunker_params,
            stats_counter: stats_counter,
        }
    }

    pub fn spawn(self) -> io::Result<thread::JoinHandle<()>> {
        thread::Builder::new().name("FileProcessor".to_string()).spawn(move || {
            loop {
                let dentry: Option<DirEntry>;

                // The Mutex is hold in this scope block. It needs to be
                // released before chunking, otherwise chunking does not
                // happen concurrently.
                {
                    let mut pathqueue = self.dir_entries.lock().unwrap();
                    dentry = pathqueue.pop();
                }

                let dentry = match dentry {
                    Some(x) => x,
                    None => break,
                };

                // Chunking happens here!
                let item = try_or_skip!(self.process(dentry));

                {
                    let mut items = self.items.lock().unwrap();
                    items.push(item);
                }

                {
                    let mut stats = self.stats_counter.lock().unwrap();
                    stats.files_read += 1;
                }
            }
        })
    }

    fn process(&self, dentry: DirEntry) -> io::Result<FSItem> {
        let metadata = try!(dentry.metadata());

        if metadata.is_file() {
            let mut f: File;
            let mut buffer: Vec<u8> = Vec::new();
            let file_mmap: Mmap;
            let mmap_buffer: &[u8];
            let chunker: Chunker;

            if metadata.len() == 0 {
                debug!("Reading zero length file: '{}'", dentry.path().display());
                return Ok(FSItem::new(dentry.clone(), metadata.clone()));
            }

            // 25 MB
            if metadata.len() < 25000000 {
                debug!("Reading file: '{}'", dentry.path().display());
                f = try!(File::open(dentry.path()));
                try!(f.read_to_end(&mut buffer));
                chunker = Chunker::new(&buffer, &self.chunker_params);
            } else {
                debug!("Memory mapping file: '{}'", dentry.path().display());
                file_mmap = try!(Mmap::open_path(dentry.path(), Protection::Read));
                mmap_buffer = unsafe { file_mmap.as_slice() };
                chunker = Chunker::new(mmap_buffer, &self.chunker_params);
            }

            let mut offsets = vec![];
            let mut hashes = vec![];
            let mut last_chunk_index = 0;

            for chunk in chunker {
                // The hasher must be recreated for each chunk, since otherwise it
                // is used to somehow stream the data and the block hashes do not
                // match. Very strange...
                let mut hasher = Sha256::new();
                hasher.input(chunk);
                last_chunk_index += chunk.len();

                offsets.push(last_chunk_index);
                hashes.push(hasher.result().to_vec());
            }

            {
                let mut stats = self.stats_counter.lock().unwrap();
                stats.bytes_read += metadata.len();
            }

            Ok(FSItem::new_with_chunks(dentry.clone(), metadata.clone(), offsets, hashes))
        } else if metadata.is_dir() {
            debug!("Reading directory: '{}'", dentry.path().display());
            Ok(FSItem::new(dentry.clone(), metadata.clone()))
        } else {
            Err(Error::new(ErrorKind::Other, format!("Unsupported filetype '{}'", dentry.path().display())))
        }
    }
}
