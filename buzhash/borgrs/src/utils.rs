use std::process::Command;

pub fn number_of_cpus() -> u32 {
    let nproc = match Command::new("nproc").output() {
        Ok(x) => x,
        Err(e) => {
            warn!("Could not spawn nproc: {}", e);
            warn!("Operating single threaded.");
            return 1;
        }
    };

    let nproc = match String::from_utf8(nproc.stdout) {
        Ok(x) => x,
        Err(e) => {
            warn!("Error parsing nproc output: {}", e);
            warn!("Operating single threaded.");
            return 1;
        }
    };

    match nproc.trim().parse::<u32>() {
        Ok(x) => x,
        Err(e) => {
            warn!("Error parsing nproc output: {}", e);
            warn!("Operating single threaded.");
            1
        }
    }
}

pub fn human_readable_byte_count(x: u64) -> String {
        let base = 1024_f64;
    	let units = vec!["kiB", "MiB", "GiB", "TiB", "PiB", "EiB"];
    	let a = x as f64;

    	if a < base {
    		return format!("{:.0} B", a);
    	}

    	// https://en.wikipedia.org/wiki/Binary_prefix
    	let exp = (a.log2() / base.log2()).floor();
        format!("{:.0} {}", a/base.powf(exp), units[exp as usize - 1])
}
