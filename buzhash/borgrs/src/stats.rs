#[derive(Debug)]
pub struct Statistics {
    pub files_read: u64,
    pub bytes_read: u64,
}

impl Statistics {
    pub fn new() -> Self {
        Self {
            files_read: 0,
            bytes_read: 0,
        }
    }
}
