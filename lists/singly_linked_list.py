class Node:

    def __init__(self, data=None, next_=None):
        self.data = data
        self.next_ = next_


class SinglyLinkedList:

    def __init__(self, head=None, tail=None):
        self.head = head
        self.tail = tail
        self.size = 0

    def insert(self, node):
        pass

    def push(self, new_node):
        if self.size == 0:
            self.head = new_node
            self.size += 1
            return

        node = self.head
        while node is not None:
            if node.next_ is None:
                self.size += 1
                node.next_ = new_node
                return
            node = node.next_

    def __iter__(self):
        if self.head is None:
            raise StopIteration
        node = self.head
        yield node
        while node.next_ is not None:
            node = node.next_
            yield node
        raise StopIteration


list_ = SinglyLinkedList()
list_.push(Node(data=5))
list_.push(Node(data=6))
list_.push(Node(data=7))
list_.push(Node(data=8))
list_.push(Node(data=5))
list_.push(Node(data=5))

for node in list_:
    print(node.data)
