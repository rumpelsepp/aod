#include "stdio.h"
#include "stdlib.h"
#include "errno.h"

struct node {
    int data;
    struct node *next;
};

struct node *list_init(int data) {
    struct node *head = malloc(sizeof(struct node));
    if (head == NULL) {
        return NULL;
    }

    head->data = data;
    head->next = NULL;

    return head;
}

int list_push(struct node *head, int data) {
    struct node *cur = head;

    while (cur->next != NULL) {
         cur = cur->next;
    }

    struct node *new_node = malloc(sizeof(struct node));
    if (new_node == NULL) {
        return -1;
    }

    new_node->data = data;
    new_node->next = NULL;
    cur->next = new_node;

    return 0;
}

int list_pop(struct node **head) {
    if (*head == NULL) {
        return -1;
    }

    struct node **pp = head;

    while ((*pp)->next != NULL) {
         pp = &(*pp)->next;
    }

    free(*pp);
    *pp = (*pp)->next;

    return 0;
}

void list_print(struct node *head) {
    struct node *cur = head;

    while (cur != NULL) {
         printf("%i ", cur->data);
         cur = cur->next;
    }

    printf("\n");
}

int main(int argc, char const **argv) {
    struct node *list = list_init(1);

    if (list == NULL) {
        return EXIT_FAILURE;
    }

    list_push(list, 2);
    list_push(list, 3);
    list_push(list, 4);
    list_push(list, 5);

    list_print(list);
    list_pop(&list);
    list_print(list);
    list_pop(&list);
    list_print(list);
    list_pop(&list);
    list_print(list);
    list_pop(&list);
    list_print(list);
    list_pop(&list);
    list_print(list);
    list_pop(&list);
    list_print(list);

    return EXIT_SUCCESS;
}
