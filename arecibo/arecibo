#!/usr/bin/python3

import base64
import math
import sys

FILLCHAR = '█'


def prime_facs(x):
    a = x
    res = []

    while a > 1:
        until = int(math.sqrt(a)) + 1
        for i in range(2, until):
            if a % i == 0:
                res.append(i)
                a /= i
                break
        else:
            res.append(int(a))
            return res
    return res


def print_vbar(x):
    print('+', end='')
    print('-' * x, end='')
    print('+')


def decode_base64_if_needed(data):
    # Data is base64.
    try:
        data = data.decode()
    # Data is in Mr.Stanner format.
    except UnicodeDecodeError:
        if data[:5].decode() != 'START':
            print('error: invalid data; START marker is missing')
            sys.exit(1)
        if data[-5:-2].decode() != 'END':
            print('error: invalid data; END marker is missing')
            sys.exit(1)

        return data
    return base64.b64decode(data)


def render(data):
    payload = data[5:-5]
    offset = int(data[-2:], 16)
    n_max = len(payload)*8 + offset
    y_screen, x_screen = prime_facs(n_max)

    print_vbar(x_screen)
    print('|', end='')

    y = 0
    x = 0
    n = 0
    for byte in payload:
        mask = 0x80
        while mask > 0 and n < n_max:
            if byte & mask:
                print(FILLCHAR, end='')
            else:
                print(' ', end='')

            x = (x + 1) % x_screen
            if x == 0:
                y += 1
                print('|')
                if y < y_screen:
                    print('|', end='')
            n += 1
            mask >>= 1

    print_vbar(x_screen)


def main():
    data = decode_base64_if_needed(sys.stdin.buffer.read())
    render(data)


if __name__ == '__main__':
    main()
