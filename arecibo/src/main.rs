use std::io;
use std::io::Read;

use base64;

fn prime_factors(x: u32) -> Vec<u32> {
    let mut a = x;
    let mut res = vec![];

    while a > 1 {
        let mut found = false;
        let until = (a as f32).sqrt() + 1_f32;
        for i in 2..until as u32 {
            if a % i == 0 {
                res.push(i);
                a /= i;
                found = true;
                break;
            }
        }

        if !found {
            res.push(a);
            return res;
        }
    }

    res
}

fn print_vbar(x: u32) {
    print!("+");
    for _ in 0..x {
        print!("-");
    }
    println!("+");
}

fn parse_decorators(buf: &[u8]) -> io::Result<isize> {
    if &buf[..5] != b"START" {
        return Err(io::Error::new(io::ErrorKind::Other, "missing START marker"));
    }

    if &buf[buf.len()-5..buf.len()-2] != b"END" {
        return Err(io::Error::new(io::ErrorKind::Other, "missing END marker"));
    }

    let offset_str = std::str::from_utf8(&buf[buf.len()-2..])
                        .map_err(|e| io::Error::new(io::ErrorKind::Other, e))?;

    let offset: isize = offset_str.parse()
                        .map_err(|e| io::Error::new(io::ErrorKind::Other, e))?;

    Ok(offset)
}

fn render(buf: &[u8], offset: isize) -> io::Result<()> {
    let data = &buf[5..buf.len()-5];

    let n_max = (data.len() as isize * 8 + offset) as usize;
    let primes = prime_factors(n_max as u32);
    if primes.len() != 2 {
        return Err(io::Error::new(io::ErrorKind::Other, "data is corrupt"));
    }

    let y_screen = primes[0];
    let x_screen = primes[1];

    print_vbar(x_screen);
    print!("|");

    let mut x = 0;
    let mut y = 0;
    let mut n = 0;
    for byte in data {
        let mut mask = 0x80;

        while mask > 0 && n < n_max {
            if (byte & mask) != 0 {
                print!("█");
            } else {
                print!(" ");
            }

            x = (x + 1) % x_screen;
            if x == 0 {
                y += 1;
                println!("|");
                if y < y_screen {
                    print!("|");
                }
            }

            n += 1;
            mask >>= 1;
        }
    }

    print_vbar(x_screen);

    Ok(())
}

fn main() -> io::Result<()> {
    let mut buf = vec![];
    io::stdin().read_to_end(&mut buf)?;

    let data = match std::str::from_utf8(&buf) {
        Ok(s) => base64::decode(&s.trim())
                    .map_err(|e| io::Error::new(io::ErrorKind::Other, e))?,
        Err(_) => buf,
    };

    let offset = parse_decorators(&data)?;

    render(&data, offset)
}
