#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

void base64_decode(char *s, size_t len) {
	static const char decoding[] = {62,0,0,0,63,52,53,54,55,56,57,58,59,60,61,0,0,0,0,0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,0,0,0,0,0,0,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51};

	for (size_t i = 0; i < len; i += 4) {
		uint32_t val = 0;

		for (size_t j = 0; j < 4; j++) {
			val |= (decoding[s[i+j]-'+'] & 0x3f) << ((3-j) * 6);
		}

		for (size_t j = 0; j < 3; j++) {
			char dec_byte = (val >> ((2-j) * 8)) & 0xff;
			printf("%c", dec_byte);
		}
	}
}

int main(int argc, char *argv[]) {
	char *input = "TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhpcyByZWFzb24sIGJ1dCBieSB0aGlz\
IHNpbmd1bGFyIHBhc3Npb24gZnJvbSBvdGhlciBhbmltYWxzLCB3aGljaCBpcyBhIGx1c3Qgb2Yg\
dGhlIG1pbmQsIHRoYXQgYnkgYSBwZXJzZXZlcmFuY2Ugb2YgZGVsaWdodCBpbiB0aGUgY29udGlu\
dWVkIGFuZCBpbmRlZmF0aWdhYmxlIGdlbmVyYXRpb24gb2Yga25vd2xlZGdlLCBleGNlZWRzIHRo\
ZSBzaG9ydCB2ZWhlbWVuY2Ugb2YgYW55IGNhcm5hbCBwbGVhc3VyZS4=";

	base64_decode(input, strlen(input));

	return EXIT_SUCCESS;
}
