import math

primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]

a = 0
b = len(primes)
i = int((b - a) / 2)
to_match = 13
j = 0
max_steps = int(math.log(b+1, 2))

while primes[i] != to_match:
    i = int((b - a) / 2) + a
    if primes[i] < to_match:
        a = i + 1
    if primes[i] > to_match:
        b = i - 1
    j += 1

    if j == max_steps:
        print("not avail")
        break

print("Found index: {}".format(i))
print("Found element: {}".format(primes[i]))
print("Required steps: {}".format(j))
