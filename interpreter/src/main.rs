use std::io;
use std::io::Read;

#[derive(Clone, Debug, PartialEq)]
enum Token {
    INTEGER(i64),
    PLUS,
    MINUS,
    SLASH,
    ASTERISK,
    ERROR(String),
    EOF,
    WHITESPACE,
}

#[derive(Clone, Debug, PartialEq)]
enum Op {
    SUB,
    ADD,
    DIV,
    MUL,
}

#[derive(Clone, Debug)]
struct Tokenizer {
    text: String,
    pos: usize,
    cur_token: Option<Token>,
}

impl Tokenizer {
    fn new(text: String) -> Self {
        Self{
            text: text,
            pos: 0,
            cur_token: None,
        }
    }
}

impl Iterator for Tokenizer {
    type Item = Token;

    fn next(&mut self) -> Option<Token> {
        let cur_char = match self.text.chars().nth(self.pos) {
            Some(v) => v,
            None => return None,
        };

        // The iterator is drained, when the EOF token is reached.
        if let Some(ref tok) = self.cur_token {
            match tok {
                Token::ERROR(_) => return None,
                Token::EOF => return None,
                _ => {},
            }
        }

        if self.pos > self.text.len() + 1 {
            self.cur_token = Some(Token::EOF);
            return self.cur_token.clone();
        }

        if cur_char.is_whitespace() {
            self.cur_token = Some(Token::WHITESPACE);
            self.pos += 1;
            return self.cur_token.clone();
        }

        if cur_char.is_digit(10) {
            let digit = cur_char.to_digit(10).unwrap();
            self.cur_token = Some(Token::INTEGER(digit as i64));
            self.pos += 1;
            return self.cur_token.clone();
        }

        if cur_char == '+' {
            self.cur_token = Some(Token::PLUS);
            self.pos += 1;
            return self.cur_token.clone();
        }

        if cur_char == '-' {
            self.cur_token = Some(Token::MINUS);
            self.pos += 1;
            return self.cur_token.clone();
        }

        if cur_char == '*' {
            self.cur_token = Some(Token::ASTERISK);
            self.pos += 1;
            return self.cur_token.clone();
        }

        if cur_char == '/' {
            self.cur_token = Some(Token::SLASH);
            self.pos += 1;
            return self.cur_token.clone();
        }

        let err_msg = format!("unexpected token: '{}'", cur_char).to_string();
        self.cur_token = Some(Token::ERROR(err_msg));
        self.cur_token.clone()
    }
}

#[derive(Clone, Debug)]
struct Interpreter {
    tokenizer: Tokenizer,
}

impl Interpreter {
    fn new(text: String) -> Self {
        Self{
            tokenizer: Tokenizer::new(text),
        }
    }

    fn next_relevant_token(&mut self, tokenizer: &mut Tokenizer) -> Token {
        for token in tokenizer {
            match token {
                Token::WHITESPACE => continue,
                _ => {},
            }

            return token;
        }

        unreachable!();
    }

    fn expr(&mut self) -> i64 {
        let mut tokenizer = self.tokenizer.clone();

        let left = match self.next_relevant_token(&mut tokenizer) {
            Token::INTEGER(i) => i,
            _ => panic!("syntax error"),
        };

        let op = match self.next_relevant_token(&mut tokenizer) {
            Token::PLUS => Op::ADD,
            Token::MINUS => Op::ADD,
            Token::SLASH => Op::DIV,
            Token::ASTERISK => Op::MUL,
            _ => panic!("syntax error"),
        };

        let right = match self.next_relevant_token(&mut tokenizer) {
            Token::INTEGER(i) => i,
            _ => panic!("syntax error"),
        };

        match op {
            Op::ADD => left + right,
            Op::SUB => left - right,
            Op::MUL => left * right,
            Op::DIV => left / right,
        }
    }
}

fn main() -> io::Result<()> {
    let mut buf = String::new();
    io::stdin().read_to_string(&mut buf)?;

    let mut interpreter = Interpreter::new(buf.clone());
    println!("Input: {}", buf.trim());
    println!("Result: {}", interpreter.expr());

    Ok(())
}
